#ifndef _CONFIGURATION_H_
#define _CONFIGURATION_H_

#include <stdint.h>


/******         APP parameters            *********/
// Schedule TX every this many seconds (might become longer due to duty
// cycle limitations).
const unsigned TX_INTERVAL_SEC = 2 * 60 + 30;

// Cayenne LPP channels
const uint8_t lpp_channel_pm10 = 1;
const uint8_t lpp_channel_pm25 = 2;


/******         LoRaWAN Keys            *********/
// This EUI must be in little-endian format, so least-significant-byte
// first. When copying an EUI from ttnctl output, this means to reverse
// the bytes. For TTN issued EUIs the last bytes should be 0xD5, 0xB3,
// 0x70.
static const u1_t PROGMEM APPEUI[8]={ 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
// This should also be in little endian format, see above.
static const u1_t PROGMEM DEVEUI[8]={ 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
// This key should be in big endian format (or, since it is not really a
// number but a block of memory, endianness does not really apply). In
// practice, a key taken from ttnctl can be copied as-is.
static const u1_t PROGMEM APPKEY[16] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

/******         PINS            *********/
// Pins for TTGO LoRa V1/V2 are defined in:
// <HOME>/.platformio/packages/framework-arduinoespressif32/variants/ttgo-lora32-v1/pins_arduino.h
// <HOME>/.platformio/packages/framework-arduinoespressif32/variants/ttgo-lora32-v2/pins_arduino.h

// SDS 011
const uint8_t PIN_SDS_RX = 23;
const uint8_t PIN_SDS_TX = 13;

// SX127x
const uint8_t PIN_LMIC_NSS = LORA_CS;
const uint8_t PIN_LMIC_RST = LORA_RST;
const uint8_t PIN_LMIC_DIO0 = LORA_IRQ;
const uint8_t PIN_LMIC_DIO1 = 33;
const uint8_t PIN_LMIC_DIO2 = 32;

// OLED screen
const uint8_t PIN_OLED_SDA = OLED_SDA;
const uint8_t PIN_OLED_SCL = OLED_SCL;
const uint8_t PIN_OLED_RST = OLED_RST;


/******         SREEN            *********/
// OLED screen for TTGO LoRa V2.1
const uint8_t SCREEN_WIDTH_PX = 128; // Display width, in pixels
const uint8_t SCREEN_HEIGHT_PX = 64; // Display height, in pixels

#endif // _CONFIGURATION_H_